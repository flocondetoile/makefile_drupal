# Makefile for Drupal 8 module contrib

## Contrib features

### Deploy

* Import content with default content deploy
* Setup Environment indicator

### Contrib command

*        make dcd-import-all
*        make set-env-indicator
