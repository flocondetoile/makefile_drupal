#!/usr/bin/make -f

#/// Get project root, only works if makefile is in the docroot directory
#/// ----------------
MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIRE := $(dir $(mkfile_path))

##### Add common command (Required)
##### ----------------------------
include Common/Makefile

##### Add contrib command (optional)
##### ------------------------------

include Contrib/default_content_deploy/Makefile
include Contrib/environment_indicator/Makefile

##### Add quality tools command (optional)
##### -----------------------------------
include Quality-tools/Makefile
