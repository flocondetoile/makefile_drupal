##### phpCodesniffer Configuration
##### ----------------------------

# The file extensions to test.
PHPCS_EXTENSIONS = php inc module install info test profile theme css js

# The default configuration file to generate.
PHPCS_CONFIG = ${PROJECT_ROOT}/phpcs.xml

# The coding standard to use. If you want to customize the rules, make a copy of
# the file and name it 'phpcs-ruleset.xml'. Then copy this property to your
# build.properties file and remove the '.dist' suffix.
PHPCS_STANDARD = ${PROJECT_ROOT}/phpcs-ruleset.xml.dist

# Paths to check, delimited by semicolons.
PHPCS_FILES = ${WEBSITE_MODULES_DIR};${WEBSITE_THEMES_DIR};${WEBSITE_PROFILES_DIR}

# Paths to ignore, delimited by semicolons.
PHPCS_IGNORE = ${WEBSITE_MODULES_DIR}/contrib;${WEBSITE_THEMES_DIR}/contrib;${WEBSITE_PROFILES_DIR}/contrib

# The report format. For example 'full', 'summary', 'diff', 'xml', 'json'.
PHPCS_REPORT = full

# Whether or not to show sniff codes in the report.
PHPCS_SNIFFCODES = 0

# Whether or not to show the progress of the run.
PHP_CS_PROGRESS = 1

define CONFIG_TEXT
<?xml version="1.0" encoding="UTF-8"?>
<ruleset name="pbm_default">
  <description>Default PHP CodeSniffer configuration for Platform Building Materials.</description>
  <rule ref="${PHPCS_STANDARD}"/>
  <file>${WEBSITE_MODULES_DIR}</file>
  <file>${WEBSITE_THEMES_DIR}</file>
  <file>${WEBSITE_PROFILES_DIR}</file>
  <arg name="extensions" value="${PHPCS_EXTENSIONS}"/>
  <exclude-pattern>${WEBSITE_MODULES_DIR}/contrib</exclude-pattern>
  <exclude-pattern>${WEBSITE_THEMES_DIR}/contrib</exclude-pattern>
  <exclude-pattern>${WEBSITE_PROFILES_DIR}/contrib</exclude-pattern>
  <arg name="report" value="${PHPCS_REPORT}"/>
  <arg value="p"/>
</ruleset>
endef

# The location of the file containing the global configuration options.
PHPCS_GLOBAL_CONFIG = VENDOR_ROOT/squizlabs/php_codesniffer/CodeSniffer.conf

# Whether or not to run a coding standards check before doing a git push. Note
# that this will abort the push if the coding standards check fails.
PHPCS_PREPUSH_ENABLE = 0

# The source and destination paths of the git pre-push hook.
PHPCS_PREPUSH_SOURCE = ${project.basedir}/phpcs-pre-push/pre-push
PHPCS_PREPUSH_DESTINATION = ${PROJECT_ROOT}/../.git/hooks/pre-push

##### Grumphp Configuration
##### ---------------------

############################################################
### OUTSIDE DOCKER (git dir not available inside docker) ###
############################################################

# This path will be used outside of your docker, also put the path in terms of your local system.
##########################
### LOCAL PREREQUISITES ##
#########################
## Php installed.
## Make installed. For windows see README.md

##### Linux example
##### -------------

#GRUMPHP_VENDOR_PATH=~/my_project/src/vendor
#GRUMPHP_DRUPAL_PATH=~/my_project/src/web

##### Windows example
##### ---------------
# GRUMPHP_VENDOR_PATH=C:/my_project/src/vendor
# GRUMPHP_DRUPAL_PATH=C:/my_project/src/web


#######################################################
### INSIDE DOCKER (git dir available inside docker) ###
######################################################

GRUMPHP_VENDOR_PATH=${PROJECT_ROOT}/vendor
GRUMPHP_DRUPAL_PATH=${PROJECT_ROOT}/web


GRUMPHP=${GRUMPHP_VENDOR_PATH}/bin/grumphp
