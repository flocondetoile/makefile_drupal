# Makefile for Drupal 8 quality tools

## Quality tools

### GrumPhp

#### Prerequisites

##### 1) Git directory mount on your instance

If you are on virtual machine (like docker) and you can't access to your .git directory on your web server, got directly to the second part

##### 2) Git directory NOT mount on your instance

If you can't access to your directory you must to install Grumphp on your local machine.

For grumphp features you need to have installed php >= 7.0 on your operating system

###### Install Make on windows

Basically on Windows make command is unknown so you can get by :
- If you have Visual Studio, use it to execute make : https://docs.microsoft.com/fr-fr/cpp/build/reference/nmake-reference?redirectedfrom=MSDN&view=vs-2019
- Install Cgwin (a Unix alike environment for Windows) : http://www.cygwin.com
- Install GnuWin32 : http://gnuwin32.sourceforge.net/
- Install MinGW : http://www.mingw.org/

### Code sniffer

### Php Unit

### Behat
*        make setup-php-codesniffer (depreciated used grumphp)
*        make setup-phpunit
*        make setup-behat
